// Τα ερωτήματα 2 έως 7 θα απαντηθούν στο αρχείο αυτό

const newGuess = document.querySelector("#new-guess");
const message = document.querySelector("#message");
const lowHigh = document.querySelector("#low-high");
const checkButton = document.querySelector("#check");
const restartButton = document.querySelector("#restart");
const root = document.querySelector(":root");

// 2. να ορίσετε τους σχετικούς χειριστές συμβάντων


newGuess.addEventListener("keyup", checkKey);

checkButton.addEventListener("click",checkGuess);

restartButton.addEventListener("click", restart);


let previousGuesses = [];
let theGuess;
window.onload = newRandom();
newGuess.focus();

//using the custom colors from CSS file
var rs = getComputedStyle(root);

var msgColorWrong = rs.getPropertyValue('--msg-wrong-color');
var msgWinColor = rs.getPropertyValue('--msg-win-color');
var msgTextColor = rs.getPropertyValue('--msg-text');
var msgLowHighColor = rs.getPropertyValue('--low-high-color');

function newRandom(){
/* 3. συνάρτηση που βρίσκει ένα τυχαίο αριθμό μεταξύ 1 και 100
 και τον εκχωρεί στη μεταβλητή theGuess */
 theGuess=Math.floor(Math.random() * 100) + 1;
 restartButton.remove();
}

function checkKey(e){
/* 4. συνάρτηση που όταν ο χρήστης πατήσει <<enter>>
 να καλεί τη συνάρτηση που αποτελεί τον κεντρικό ελεγκτή του παιχνιδιού.
 */
if (e.code==='Enter'){
    checkGuess();
}
}


function checkGuess(){
/* 5. Να ορίσετε συνάρτηση checkGuess η οποία καλείται είτε όταν ο χρήστης πατήσει <<enter>>
στο πεδίο "new-guess" είτε όταν πατήσει το πλήκτρο "check", η οποία είναι ο κεντρικός ελεγκτής,
καλεί τη συνάρτηση processGuess (η οποία αποφαίνεται για την ορθότητα του αριθμού) και κάνει
τις κατάλληλες ενέργειες για να μην μπορεί να εισάγει ο χρήστης νέο αριθμό ή να ανασταλεί η
λειτουργία του <<enter>>, εμφάνιση του πλήκτρου 'restart' και την εξαφάνιση του πλήκτρου 'check'
σε περίπτωση ολοκλήρωσης του παιχνιδιού. */

let status = processGuess(newGuess.value);
if (status=="win" || status=="lost"){
    newGuess.removeEventListener("keyup",checkKey);
    checkButton.remove();
    root.appendChild(restartButton);
    return false;
}
}


function processGuess(newValue){
 /* 6.  Να ορίσετε συνάρτηση processGuess(newValue) η οποία καλείται από τη συνάρτηση checkGuess,
 περιέχει τη λογική του παιχνιδιού, ελέγχει αν η τιμή του χρήστη είναι σωστή, ή αν το παιχνίδι έχει
 τελειώσει χωρίς ο χρήστης να έχει βρει τον αριθμό, και επιστρέφει αντίστοιχα την τιμή "win", ή "lost",
 δημιουργεί και εμφανίζει τα κατάλληλα μηνύματα, αλλάζοντας το χρώμα του στοιχείου μηνυμάτων.
 Όλα τα μηνύματα του προγράμματος εμανίζονται από την processGuess().
 Σε περίπτωση που το παιχνίδι δεν έχει ακόμα τελειώσει, η συνάρτηση μπορεί είτε να μην επιστρέφει κάποια ιδιαίτερη τιμή,
 είτε να επιστρέφει κάποια τιμή της επιλογής σας */
 if (isNaN(newValue) || (newValue && !newValue.trim())){
    message.textContent='Δώσε αριθμό!';
    message.style.color=msgTextColor;
    message.style.fontSize='25px'; 
    message.style.backgroundColor=msgColorWrong;
    message.style.textAlign='center';

 }

 else if (newValue>theGuess || newValue>100){
    message.textContent='Λάθος, το ξεπέρασες';
    message.style.color=msgTextColor;
    message.style.fontSize='25px'; 
    message.style.backgroundColor=msgColorWrong; 
    message.style.textAlign='center';


    previousGuesses.push(newValue);
    newGuess.value='';

    lowHigh.textContent=`Προηγούμενες προσπάθειες: ${previousGuesses}`;
    lowHigh.style.color=msgLowHighColor;
    lowHigh.style.fontSize='20px';
    if (previousGuesses.length==10){
        message.textContent='Τέλος παιχνιδιού, έχασες!';  
        message.style.color=msgTextColor;
        message.style.fontSize='25px'; 
        message.style.backgroundColor=msgColorWrong;   
        message.style.textAlign='center';
  
        return "lost";
    }

}
else if(newValue<theGuess || newValue<1){
    message.textContent='Λάθος, είσαι πιο χαμηλά'; 
    message.style.color=msgTextColor;
    message.style.fontSize='25px'; 
    message.style.backgroundColor=msgColorWrong; 
    message.style.textAlign='center';


    previousGuesses.push(newValue);
    newGuess.value='';

    lowHigh.textContent=`Προηγούμενες προσπάθειες: ${previousGuesses}`;
    lowHigh.style.color=msgLowHighColor;
    lowHigh.style.fontSize='20px';
    if (previousGuesses.length==10){
        message.textContent='Τέλος παιχνιδιού, έχασες!';
        message.style.color=msgTextColor;
        message.style.fontSize='25px'; 
        message.style.backgroundColor=msgColorWrong; 
        message.style.textAlign='center';
        return "lost";
    }
}

else if (newValue==theGuess) {
    message.textContent='Μπράβο το βρήκες!';
    message.style.backgroundColor=msgWinColor;
    message.style.fontSize='25px'; 
    message.style.color=msgTextColor;
    message.style.textAlign='center';

    newGuess.value='';
    return "win";
}

}


function restart(){
/* 7. Να ορίσετε συνάρτηση restart η οποία καλείται όταν ο χρήστης πατήσει το πλήκτρο
'restart' και επανεκινεί τη διαδικασία */
window.location.reload();
}
